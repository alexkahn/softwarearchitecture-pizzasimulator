package pizzashop

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl._


trait PizzaShopSim extends App {

  implicit val actorSystem = ActorSystem("Pizza Simulator")

  val streamDecider: Supervision.Decider = {
    case e: Exception => {
      println("An error occurred when processing the stream: " + e.getMessage())
      Supervision.Stop
    }
  }
  val actorMaterializerSettings = ActorMaterializerSettings(actorSystem)
    .withSupervisionStrategy(streamDecider)
  implicit val actorMaterializer = ActorMaterializer(actorMaterializerSettings)




  // cases
  case class EatInOrder(id: Int, takeOut: Boolean)
  case class TakeOutOrder(id: Int, takeOut: Boolean)

  case class EatInBatch(id: Int, takeOut: Boolean)
  case class TakeOutBatch(id: Int, takeOut: Boolean)

  case class CountedOrder(id: Int, takeOut: Boolean)

  case class PreparedPizza(id: Int, takeOut: Boolean)
  case class RawPizza(id: Int, takeOut: Boolean)
  case class BakedPizza(id: Int, takeOut: Boolean)
  case class BoxedPizza(id: Int, takeOut: Boolean)


  // stages

  def orderStageEI = Flow[EatInOrder].map(order => EatInBatch(order.id, order.takeOut))
  def orderStageTO = Flow[TakeOutOrder].map(order => TakeOutBatch(order.id, order.takeOut))

  val batchStageEI = Flow[EatInBatch].grouped(2)
  val batchStageTO = Flow[TakeOutBatch].grouped(4)

  def countStageEI = Flow[Seq[EatInBatch]].map(
    orders => orders. map (order => {
      println("Counted a pizza to eat in. It's number is: " + order.id)
      CountedOrder(order.id, order.takeOut)
    })
  )
  def countStageTO = Flow[Seq[TakeOutBatch]].map(
    orders => orders. map (order => {
        println("Counted a pizza to take out. It's number is: " + order.id)
        CountedOrder(order.id, order.takeOut)
    })
  )

  def printTypeStage = Flow[Seq[CountedOrder]].map(
    orders => orders. map (order => {
      if (order.takeOut) {
        println("This is a takeout order")
      }
      else
      println("This is an eat in order")
      CountedOrder(order.id, order.takeOut)
    })
  )

  def preparePizzaStageEI = Flow[Seq[EatInBatch]].map( orders => orders.map (order => PreparedPizza(order.id, order.takeOut)))
  def preparePizzaStageTO = Flow[Seq[TakeOutBatch]].map( orders => orders.map (order => PreparedPizza(order.id, order.takeOut)))
  def preparePizzaStage = Flow[Seq[PreparedPizza]].map( orders => orders.map (order => RawPizza(order.id, order.takeOut)))

  def ovenCreator(cooking: Int) = {
    Flow[Seq[RawPizza]].map(pizzas => pizzas.map( rawpizza => {
      println("Raw pizza entered oven")
      Thread.sleep(cooking)
      println("Baked pizza exited oven ")
      BakedPizza(rawpizza.id, rawpizza.takeOut)
    }))
  }

  val oldOven = ovenCreator(7000)
  val newOven0 = ovenCreator(3000)
  val newOven1 = ovenCreator(3000)

  val ovens = Array(oldOven, newOven0, newOven1)

  def rackStageEI = Flow[Seq[BakedPizza]].map( orders => orders.map (order => BoxedPizza(order.id, order.takeOut)))
  def rackStageTO = Flow[Seq[BakedPizza]].map( orders => orders.map (order => BoxedPizza(order.id, order.takeOut)))

  def printPizzaBoxedStage = Flow[Seq[BoxedPizza]].map( orders => orders.map (box => {
    println("Pizza is ready to be served. ID: " + box.id)
    BoxedPizza(box.id, box.takeOut)
  }))

    val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
      import GraphDSL.Implicits._

    val eatInOrders = (1 to 5).map(i => (EatInOrder(i, false)))
    val takeOutOrders = (6 to 10).map(i => (TakeOutOrder(i, true)))

    val takeOutsource = Source(takeOutOrders)
    val eatInSource = Source(eatInOrders)

      val broadcastOrderTO = builder.add(Broadcast[Seq[TakeOutBatch]](2))
      val broadcastOrderEI = builder.add(Broadcast[Seq[EatInBatch]](2))
      val mergeCounterbranch = builder.add(Merge[Seq[CountedOrder]](2))
      val mergeBakingbranch = builder.add(Merge[Seq[PreparedPizza]](2))
      val mergeRackbranch = builder.add(Merge[Seq[BoxedPizza]](2))


      val prepareOrders = builder.add(Balance[Seq[RawPizza]](3))
      val mergeBakedPizza = builder.add(Merge[Seq[BakedPizza]](3))

      prepareOrders.out(0) ~> ovens(0) ~> mergeBakedPizza.in(0)
      prepareOrders.out(1) ~> ovens(1) ~> mergeBakedPizza.in(1)
      prepareOrders.out(2) ~> ovens(2) ~> mergeBakedPizza.in(2)

      val bakePizza = FlowShape(prepareOrders.in, mergeBakedPizza.out)

      val sendToRack = builder.add(Partition[Seq[BakedPizza]](2, pizzas => {
        if (pizzas.head.takeOut) {
          0
        }
        else {
          1
        }
      }))

      val toEatInRack = FlowShape(sendToRack.in, sendToRack.out(0))
      val toTakeOutRack = FlowShape(sendToRack.in, sendToRack.out(1))


      var out = Sink.ignore
      val outlet = Sink.foreach(println)

      eatInSource ~> orderStageEI ~> batchStageEI ~>  broadcastOrderEI
      takeOutsource ~> orderStageTO ~> batchStageTO ~> broadcastOrderTO

      // To counter
      broadcastOrderEI ~> countStageEI ~> mergeCounterbranch ~> printTypeStage~> out
      broadcastOrderTO ~> countStageTO ~> mergeCounterbranch

      // To ovens
      broadcastOrderEI ~> preparePizzaStageEI ~> mergeBakingbranch ~> preparePizzaStage ~> bakePizza ~> sendToRack
      broadcastOrderTO ~> preparePizzaStageTO ~> mergeBakingbranch

      toEatInRack ~> rackStageEI ~> mergeRackbranch ~> printPizzaBoxedStage ~> out
      toTakeOutRack ~> rackStageTO ~> mergeRackbranch

    ClosedShape
  })
  graph.run()
}








